const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../../app");
const company = require("../../models").Company;

chai.use(chaiHttp);
chai.should();

describe("Company", () => {
  describe("GET /companies", () => {
    beforeEach(function () {
      sinon.restore();
    });
    it("positive case: should responds with 200 status code if companies success without token", (done) => {
      sinon.stub(company, `findAll`).callsFake(() => {
        return;
      });
      chai
        .request(app)
        .get("/companies")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it("negative case: should responds with 500 status code if companies unsuccess without token", (done) => {
      sinon.stub(company, `findAll`).callsFake(() => {
        return error;
      });
      chai
        .request(app)
        .get("/companies")
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    });
  });

  describe("POST /companies", () => {
    beforeEach(function () {
      sinon.restore();
    });

    it("positive case: should responds with 201 status code if companies success without token", (done) => {
      sinon.stub(company, `create`).callsFake(() => {
        return;
      });
      chai
        .request(app)
        .post("/companies")
        .send({
          name: "nanda",
          address: "Cianjur",
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(201);

          done();
        });
    });

    it("negative case: should responds with 200 status code if companies unsuccess without token", (done) => {
      sinon.stub(company, `create`).callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post("/companies")
        .send({
          name: "nanda",
          address: "Cianjur",
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(500);

          done();
        });
    });
  });

  describe("POST /companies/create-include-employee", () => {
    beforeEach(function () {
      sinon.restore();
    });

    it("positive case: should responds with 201 status code if companies/create-include-employee success without token", (done) => {
      sinon.stub(company, `create`).callsFake(() => {
        return;
      });
      chai
        .request(app)
        .post("/companies/create-include-employee")
        .send({
          name: "Tower Bersama Group",
          address: "DKI Jakarta",
          employees: [
            {
              name: "Retno",
              phone: "08113131213",
              address: "Jakarta",
              email: "retno@tbg.com",
              password: "123",
            },
            {
              name: "Epa",
              phone: "0823132121",
              address: "Yogyakarta",
              email: "epa@tbg.com",
              password: "123",
            },
          ],
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(201);

          done();
        });
    });

    it("negative case: should responds with 200 status code if companies/create-include-employee unsuccess without token", (done) => {
      sinon.stub(company, `create`).callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post("/companies/create-include-employee")
        .send({
          name: "nanda",
          address: "Cianjur",
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(500);

          done();
        });
    });
  });
});
