const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../../app');
const employees = require('../../models').Employee

chai.use(chaiHttp);
chai.should();

describe("Employees", () => {
  describe("GET /employees", () => {
    beforeEach(function () {
      sinon.restore();
    });
    it("positive case: should responds with 200 status code if employees success without token", (done) => {
      sinon.stub(employees, `findAll`).callsFake(() => {
        return;
      });
      chai.request(app)
        .get("/employees")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    })
    it("negative case: should responds with 500 status code if employees unsuccess without token", (done) => {
      sinon.stub(employees, `findAll`).callsFake(() => {
        return error;
      });
      chai.request(app)
        .get("/employees")
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    })
  })

  describe('POST /employees', () => {
    beforeEach(function () {
      sinon.restore();
    });

    it('positive case: should responds with 200 status code if employees success without token', (done) => {
      sinon.stub(employees, `create`).callsFake(() => {
        return;
      });
      chai
        .request(app)
        .post('/employees')
        .send({
          name: "nanda",
          phone: "08112348007",
          address: "Cianjur",
          email: "nanda@mail.com",
          companyId: 1,
          password: "12345",
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(200);

          done();
        });
    });

    it('negative case: should responds with 200 status code if employees unsuccess without token', (done) => {
      sinon.stub(employees, `create`).callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post('/employees')
        .send({
          name: "nanda",
          phone: "08112348007",
          address: "Cianjur",
          email: "nanda@mail.com",
          companyId: 1,
          password: "12345",
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(500);

          done();
        });
    });
  })
})