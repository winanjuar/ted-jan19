const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../../app');
const tool = require('../../models').Tool

chai.use(chaiHttp);
chai.should();

describe("Tool", () => {
  describe("GET /tools", () => {
    beforeEach(function () {
      sinon.restore();
    });
    it("positive case: should responds with 200 status code if tools success without token", (done) => {
      sinon.stub(tool, `findAll`).callsFake(() => {
        return;
      });
      chai.request(app)
        .get("/tools")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    })
    it("negative case: should responds with 500 status code if tools unsuccess without token", (done) => {
      sinon.stub(tool, `findAll`).callsFake(() => {
        return error;
      });
      chai.request(app)
        .get("/tools")
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    })
  })
})