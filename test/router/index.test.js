const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');
const employees = require('../../models/employee')

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjo3LCJuYW1lIjoiSm9obiIsImVtYWlsIjoiY2FoeWFuZ3RpYUBnbWFpbC5jb20iLCJjb21wYW55SWQiOjF9LCJpYXQiOjE2MTI1MTM3MDZ9.Q9UDJpDWEhQQ3G0cmVzXP0xRvRdkTlETRxHRgMpmY7s"

chai.use(chaiHttp);
chai.should();

describe("Index", () => {
  it("positive case: should responds with 200 status code if index success with token", (done) => {
    chai.request(app)
      .get("/")
      .set({ Authorization: `Bearer ${token}` })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  })
})