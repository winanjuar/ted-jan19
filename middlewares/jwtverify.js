const JWT = require("jsonwebtoken");

module.exports = {
  async JWTVerify(req, res, next) {
    try {
      const token = req.headers.token;
      if (!token) {
        res.status(403).json({
          message: "Token does not exist",
        });
      } else {
        const { user } = await JWT.verify(token, process.env.SECRET_KEY);
        if (user && user.id) {
          next();
        } else {
          res.status(403).json({
            message: "Sorry, user unauthenticated",
          });
        }
      }
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
