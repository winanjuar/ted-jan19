"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Expires", [
      {
        expired: "2021-08-02",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        expired: "2021-09-15",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        expired: "2021-10-07",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Expires", null, {});
  },
};
