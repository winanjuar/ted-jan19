"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Tools", [
      {
        name: "NGSemeru",
        category: "Tools Maintenance Site",
        url: "semeru.telkomsel.co.id",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Monalisa",
        category: "Tools Monitoring Alarm",
        url: "ineom.telkomsel.co.id",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Tools", null, {});
  },
};
