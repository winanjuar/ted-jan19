"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Employees", [
      {
        name: "Sugeng Winanjuar",
        phone: "08113131213",
        address: "Cirebon",
        companyId: 1,
        email: "sugeng_d_winanjuar@telkomsel.co.id",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Nanda Pratama",
        phone: "08113131441",
        address: "Cianjur",
        companyId: 1,
        email: "nanda_c_pratama@telkomsel.co.id",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Employees", null, {});
  },
};
