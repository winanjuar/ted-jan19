"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Units", [
      {
        name: "BOX_10STRIP",
        label: "Box",
        denom: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "STRIP_10TAB",
        label: "Strip",
        denom: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "TABLET",
        label: "Tablet",
        denom: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Units", null, {});
  },
};
