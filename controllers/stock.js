const { Drug, Unit, Expire, DrugUnitExpire } = require("../models");

module.exports = {
  async list(req, res) {
    try {
      const stocks = await DrugUnitExpire.findAll({
        include: ["drug", "unit", "expire"],
      });
      res.status(200).json({
        message: "Success retrieve all stock",
        data: stocks,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async listReportByDrug(req, res) {
    try {
      const { id } = req.params;
      const drugs = await Drug.findByPk(id, {
        include: [
          {
            model: DrugUnitExpire,
            as: "stocks",
            attributes: ["quantity"],
            include: ["unit", "expire"],
          },
        ],
      });
      res.status(200).json({
        message: "Success retrieve all stock",
        data: drugs,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async listReportByUnit(req, res) {
    try {
      const { id } = req.params;
      const units = await Unit.findByPk(id, {
        include: [
          {
            model: DrugUnitExpire,
            as: "stocks",
            attributes: ["quantity"],
            include: ["drug", "expire"],
          },
        ],
      });
      res.status(200).json({
        message: "Success retrieve all stock",
        data: units,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async listReportByExpire(req, res) {
    try {
      const { id } = req.params;
      const expires = await Expire.findByPk(id, {
        include: [
          {
            model: DrugUnitExpire,
            as: "stocks",
            attributes: ["quantity"],
            include: ["drug", "unit"],
          },
        ],
      });
      res.status(200).json({
        message: "Success retrieve all stock",
        data: expires,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
