const { Employee } = require("../models");
const bcrypt = require("bcrypt");
const JWT = require("jsonwebtoken");

module.exports = {
  async login(req, res) {
    try {
      if (req.body.email && req.body.password) {
        const { email, password } = req.body;
        const employee = await Employee.findOne({ where: { email } });
        if (employee && employee.id) {
          const isAuthenticated = await bcrypt.compare(
            password,
            employee.password
          );
          if (isAuthenticated) {
            const user = {
              id: employee.id,
              name: employee.name,
              email: employee.email,
              companyId: employee.companyId,
            };
            const token = JWT.sign({ user }, process.env.SECRET_KEY);
            res.status(201).json({
              message: `Login employee using email ${email} successfully`,
              token,
            });
          } else {
            res.status(401).json({
              message: `Login employee failed due to wrong password!`,
            });
          }
        } else {
          res.status(401).json({
            message: `Sorry, employee with email ${email} not found!`,
          });
        }
      } else {
        res.status(401).json({
          message: "Sorry, email and password should not empty",
        });
      }
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
