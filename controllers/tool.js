const { Tool, Employee } = require("../models");

class ToolController {
  async list(req, res) {
    try {
      const tools = await Tool.findAll({
        include: [
          {
            model: Employee,
            as: "employees",
            attributes: ["id", "name", "address"],
            through: {
              attributes: [],
            },
            include: ["company"],
          },
        ],
      });
      res.status(200).json({
        message:
          "Success retrieve all tools with information about employee and company",
        data: tools,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  }
}

module.exports = ToolController;
