var express = require("express");
var router = express.Router();
const passport = require("passport");

const EmployeesController = require("../controllers/employee");

const employeeController = new EmployeesController()

router.post("/",
  // passport.authenticate("jwt", { session: false }),
  employeeController.create
);

router.get(
  "/",
  // passport.authenticate("jwt", { session: false }),
  employeeController.list
);

router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  employeeController.update
);

module.exports = router;
