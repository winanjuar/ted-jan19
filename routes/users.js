var express = require("express");
var router = express.Router();
const users = require("../controllers/user");

try{
  router.post("/login", users.login);
}catch(error){
  console.log(error)
}

module.exports = router;
