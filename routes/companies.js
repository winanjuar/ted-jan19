var express = require("express");
var router = express.Router();

// const companies = require("../controllers/company");

// const JWTVerify = require("../middlewares/jwtverify").JWTVerify;
// router.use(JWTVerify);

const CompanyController = require("../controllers/company");
const companyController = new CompanyController();

const passport = require("passport");

router.get(
  "/",
  // passport.authenticate("jwt", { session: false }),
  companyController.list
);

router.post(
  "/",
  // passport.authenticate("jwt", { session: false }),
  companyController.createNewComany
);

router.post(
  "/create-include-employee",
  companyController.createNewCompanyIncludeEmployee
);

module.exports = router;
