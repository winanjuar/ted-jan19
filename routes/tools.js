var express = require("express");
var router = express.Router();
const passport = require("passport");
const ToolController = require("../controllers/tool");
const toolController = new ToolController();

router.get(
  "/",
  toolController.list
);

module.exports = router;
