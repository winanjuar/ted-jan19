var express = require("express");
var router = express.Router();
const passport = require("passport");

const stocks = require("../controllers/stock");

router.get("/", passport.authenticate("jwt", { session: false }), stocks.list);
router.get(
  "/drugs/:id",
  passport.authenticate("jwt", { session: false }),
  stocks.listReportByDrug
);
router.get(
  "/units/:id",
  passport.authenticate("jwt", { session: false }),
  stocks.listReportByUnit
);
router.get(
  "/expires/:id",
  passport.authenticate("jwt", { session: false }),
  stocks.listReportByExpire
);

module.exports = router;
