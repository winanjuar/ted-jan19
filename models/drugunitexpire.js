"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class DrugUnitExpire extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Drug, {
        foreignKey: "drugId",
        as: "drug",
      });

      this.belongsTo(models.Unit, {
        foreignKey: "unitId",
        as: "unit",
      });

      this.belongsTo(models.Expire, {
        foreignKey: "expireId",
        as: "expire",
      });
    }
  }
  DrugUnitExpire.init(
    {
      drugId: DataTypes.INTEGER,
      unitId: DataTypes.INTEGER,
      expireId: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "DrugUnitExpire",
      timestamps: false,
    }
  );
  return DrugUnitExpire;
};
