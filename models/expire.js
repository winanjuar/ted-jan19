"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Expire extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.DrugUnitExpire, {
        foreignKey: "expireId",
        as: "stocks",
      });
    }
  }
  Expire.init(
    {
      expired: DataTypes.DATEONLY,
      status: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Expire",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );
  return Expire;
};
