"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class EmployeeTool extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Employee, {
        foreignKey: "employeeId",
        as: "employee",
      });

      this.belongsTo(models.Tool, {
        foreignKey: "toolId",
        as: "tool",
      });
    }
  }
  EmployeeTool.init(
    {
      employeeId: DataTypes.INTEGER,
      toolId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "EmployeeTool",
      timestamps: false,
    }
  );
  return EmployeeTool;
};
