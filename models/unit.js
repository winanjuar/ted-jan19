"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Unit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.DrugUnitExpire, {
        foreignKey: "unitId",
        as: "stocks",
      });
    }
  }
  Unit.init(
    {
      name: DataTypes.STRING,
      label: DataTypes.STRING,
      denom: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Unit",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );
  return Unit;
};
